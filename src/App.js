import React, { Component } from 'react';
import './App.css';

import Ingredient from './components/Ingredient/Ingredient';
import Burger from './components/Burger/Burger';

import meatImage from './assets/meat.png';
import cheeseImage from './assets/cheese.png';
import saladImage from './assets/salad.png';
import baconImage from './assets/bacon.png';

const INGREDIENTS = [
  {name: 'Meat', price: 50, image: meatImage},
  {name: 'Cheese', price: 20, image: cheeseImage},
  {name: 'Salad', price: 5, image: saladImage},
  {name: 'Bacon', price: 30, image: baconImage}
];

class App extends Component {
  state = {
    ingredients: [
      {name: 'Meat', count: 0},
      {name: 'Cheese', count: 0},
      {name: 'Salad', count: 0},
      {name: 'Bacon', count: 0}
    ]
  };

  getIngredientIndex = ingredientName => {
    return this.state.ingredients.findIndex((element) => {
      if (element.name === ingredientName) {
        return element;
      } else return false;
    });
  };

  addIngredient = ingredient => {
    return () => {
      let ingredients = [...this.state.ingredients];
      ingredients[this.getIngredientIndex(ingredient)].count++;

      this.setState({ingredients});
    };
  };

  removeIngredient = ingredient => {
    return () => {
      let ingredients = [...this.state.ingredients];
      const ingredientIndex = this.getIngredientIndex(ingredient);

      if (ingredients[ingredientIndex].count > 0) {
        ingredients[ingredientIndex].count--;
        this.setState({ingredients});
      }
    };
  };

  renderIngredients = () => {
    return INGREDIENTS.map((ingredient, index) => {
      return (<Ingredient
        name={ingredient.name}
        count={this.state.ingredients[this.getIngredientIndex(ingredient.name)].count}
        image={ingredient.image}
        addBtnHandler={this.addIngredient(ingredient.name)}
        removeBtnHandler={this.removeIngredient(ingredient.name)}
        key={index}
      />);
    });
  };

  getBurgerPrice = () => {
    let price = 20;

    const ingredients = this.state.ingredients;
    for (let i = 0; i < ingredients.length; i++) {
      price += ingredients[i].count * INGREDIENTS[this.getIngredientIndex(ingredients[i].name)].price;
    }

    return price;
  };

  render() {
    return (
      <div className="container">
        <div>
          <h4>Ingredients</h4>
          {this.renderIngredients()}
        </div>
        <div>
          <h4>Burger</h4>
          <Burger
            meatCount={this.state.ingredients[this.getIngredientIndex('Meat')].count}
            cheeseCount={this.state.ingredients[this.getIngredientIndex('Cheese')].count}
            saladCount={this.state.ingredients[this.getIngredientIndex('Salad')].count}
            baconCount={this.state.ingredients[this.getIngredientIndex('Bacon')].count}
          />
          <p className="burgerPrice">Price: {this.getBurgerPrice()}</p>
        </div>
      </div>
    );
  }
}

export default App;
