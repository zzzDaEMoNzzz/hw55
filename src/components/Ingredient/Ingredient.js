import React from 'react';
import './Ingredient.css';

const Ingredient = props => {
  return (
    <div className="Ingredient">
      <button className="ingredientAddBtn"><img src={props.image} alt="" onClick={props.addBtnHandler} /></button>
      <span className="ingredientName">{props.name}</span>
      <span className="ingredientCount">x{props.count}</span>
      <button className="ingredientRemoveBtn" onClick={props.removeBtnHandler}>X</button>
    </div>
  );
};

export default Ingredient;