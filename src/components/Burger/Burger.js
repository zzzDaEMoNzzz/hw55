import React from 'react';
import './Burger.css';

import BreadTop from './BreadTop/BreadTop';
import BreadBottom from './BreadBottom/BreadBottom';
import Bacon from './Bacon/Bacon';
import Salad from './Salad/Salad';
import Cheese from './Cheese/Cheese';
import Meat from './Meat/Meat';

const Burger = props => {
  const burgerFilling = () => {
    let fillingArray = [];

    for (let i = 0; i < props.baconCount; i++) {
      fillingArray.push(<Bacon key={'Bacon-' + i} />)
    }

    for (let i = 0; i < props.saladCount; i++) {
      fillingArray.push(<Salad key={'Salad-' + i} />)
    }

    for (let i = 0; i < props.cheeseCount; i++) {
      fillingArray.push(<Cheese key={'Cheese-' + i} />)
    }

    for (let i = 0; i < props.meatCount; i++) {
      fillingArray.push(<Meat key={'Meat-' + i} />)
    }

    return fillingArray;
  };

  return (
    <div className="Burger">
      <BreadTop />
      {burgerFilling()}
      <BreadBottom />
    </div>
  );
};

export default Burger;